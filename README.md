# OpenML dataset: diabetes_numeric

https://www.openml.org/d/212

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

This data set concerns the study of the factors affecting patterns of
 insulin-dependent diabetes mellitus in children.  The objective is to
 investigate the dependence of the level of serum C-peptide on the
 various other factors in order to understand the patterns of residual
 insulin secretion. The response measurement is the logarithm of
 C-peptide concentration (pmol/ml) at the diagnosis, and the predictor
 measurements age and base deficit, a measure of acidity.

 Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
 http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
 Original source: Book Generalized Additive Models (p.304) by Hastie &
 Tibshirani, Chapman & Hall.  
 Characteristics: 43 cases; 3 continuous variables

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/212) of an [OpenML dataset](https://www.openml.org/d/212). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/212/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/212/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/212/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

